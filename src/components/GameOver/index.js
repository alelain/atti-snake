import React, { PureComponent } from 'react'

import StyledButton from './styles/StyledButton'
import StyledContainer from './styles/StyledContainer'

class GameOver extends PureComponent {

  render () {
    const { resetGame, score } = this.props

    return (
      <StyledContainer>
        <h1>You are dead... You'll do better next time ;)</h1><br/>
        <h1>Score: <b>{score}</b></h1>
        <h2>Press F5 to reload the game, or hit the button below:</h2>
        <StyledButton onClick={resetGame}>I need another try!</StyledButton>
      </StyledContainer>
    )
  }
}

export default GameOver