import styled from 'styled-components'

const StyledContainer = styled.div`
  height: 100%;
  width: 100%;
  min-height: 100%;
  min-width: 100%;

  background-color: ${props => props.theme.colors.blood};
  color: ${props => props.theme.colors.snow};

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export default StyledContainer