import styled from 'styled-components'

const StyledButton = styled.button`
  margin-top: 20px;
  padding: 12px 32px;
  cursor: pointer;
  font-size: 1.2em;
  background-color: ${props => props.theme.colors.primary};
  color: ${props => props.theme.colors.snow};
  border: none;

  &:hover {
    background-color: ${props => props.theme.colors.snow};
    color: ${props => props.theme.colors.primary};
  }
`

export default StyledButton