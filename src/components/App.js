import React, { Component, Fragment } from 'react'
import { concat, head, last, map, isEqual, random, tail } from 'lodash'
import { ThemeProvider } from 'styled-components' 

import Apple from './Apple'
import Window from './Window'
import Snake from './Snake'
import GameOver from './GameOver'
import Score from './Score'

import { GameContext } from './GameProvider'

import theme from "./styles"

class App extends Component {

  state = {
    apple: {
      x: 0,
      y: 0,
    },
    snake: [
      {
        x: 100,
        y: 150,
      },
    ],
    direction: 'r',
    gameOver: false,
    gameWindow: {},
  }

  componentDidMount() {
    this.setState({
      apple:{
        x: random(0, this.gameWindow.clientWidth - this.values.bWidth),
        y: random(0, this.gameWindow.clientHeight - this.values.bHeight)
      },
      gameWindow: this.gameWindow
    })
  }

  componentDidUpdate(pp, ps) {
    const { snake } = this.state
    if (!isEqual(snake, ps.snake)) {
      const snakeHead = head(snake)
      const blocks = tail(snake)
      for (let block of blocks) {
        if (this.hasCollision(block, snakeHead)) {
          this.setGameOver(true)
          return
        }
      }
    }
  }

  getScore = () => {
    const { snake } = this.state
    return snake.length - 1
  }

  setGameOver = isOver => this.setState({ gameOver: isOver })
  
  setGameWindowRef = e => this.gameWindow = e.parentNode

  hasCollision = (blockA, blockB) => {
    const { bHeight, bWidth } = this.values
    const { x: xa, y: ya } = blockA
    const { x: xb, y: yb } = blockB
    return (
      (xa > xb && xa < xb + bWidth && ya > yb && ya < yb + bHeight) ||
      (xa + bWidth > xb && xa < xb && ya + bHeight > yb && ya < yb)
    )

  }

  addBlockToSnake = () => {
    const { snake, direction } = this.state
    const { bHeight, bWidth } = this.values
    const lastElement = last(snake)
    let blockToAdd = {}
    switch (direction) {
      case 'l':
        blockToAdd = {
          x: lastElement.x + bWidth,
          y: lastElement.y,
        }
        break;
      case 'r':
        blockToAdd = {
          x: lastElement.x - bWidth,
          y: lastElement.y,
        }
        break;
      case 't':
        blockToAdd = {
          x: lastElement.x,
          y: lastElement.y + bHeight,
        }
        break;
      case 'b':
        blockToAdd = {
          x: lastElement.x,
          y: lastElement.y - bHeight,
        }
        break;
      default:
        break;
    }
    this.setState({ 
      snake: concat(snake, [blockToAdd]),
      apple:{
        x: random(0, this.gameWindow.clientWidth - this.values.bWidth),
        y: random(0, this.gameWindow.clientHeight - this.values.bHeight)
      }
    })
  }

  moveSnake = (xb, yb, d) => {
    const { apple, snake } = this.state
    
    const newSnake = map(snake, (block, index) => {
      if (index === 0) {
        return {
          x: block.x + xb,
          y: block.y + yb,
        }
      }
      return {
        x: snake[index - 1].x,
        y: snake[index - 1].y,
      }
    })
    this.setState({ snake: newSnake, direction: d }, () => {
      if (this.hasCollision(head(newSnake), apple)) {
        this.addBlockToSnake()
      }
    })
  }

  resetGame = () => {
    const { gameWindow, apple, ...theRest } = this.intialState
    this.setState(theRest)
  }

  renderContent = () => {
    const { apple, gameOver, gameWindow, snake } = this.state
    return gameOver ? 
      (
        <GameOver score={this.getScore()} resetGame={this.resetGame}/>
      ) :
      (
        <Fragment>
          <Score score={this.getScore()} />
          <Apple x={apple.x} y={apple.y}/>
          <Snake
            tail={snake}
            moveSnake={this.moveSnake}
            setGameOver={this.setGameOver}
            windowMaxX={gameWindow.clientWidth}
            windowMaxY={gameWindow.clientHeight}
          />
        </Fragment>
      )
  }
  
  values = { bHeight: 50, bWidth: 50, speed: 100 }
  gameWindow = null
  intialState = {
    apple: {
      x: 0,
      y: 0,
    },
    snake: [
      {
        x: 100,
        y: 150,
      },
    ],
    direction: 'r',
    gameOver: false,
    gameWindow: {},
  }

  render () {

    return (
      <GameContext.Provider value={this.values}>
        <ThemeProvider theme={theme}>
          <Window setRef={this.setGameWindowRef}>
            {this.renderContent()}
          </Window>
        </ThemeProvider>
      </GameContext.Provider>
    )
  }

}

export default App