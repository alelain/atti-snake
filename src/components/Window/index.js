import React, { Component } from 'react'

import StyledWindow from './styles/StyledWindow'

class Window extends Component {

  render () {
    const { children, setRef } = this.props

    return (
      <StyledWindow innerRef={setRef}>{children}</StyledWindow>
    )
  }
}

export default Window