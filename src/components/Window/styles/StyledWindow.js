import styled from 'styled-components'

const StyledWindow = styled.div`
  background-color: ${props => props.theme.colors.primary};
  height: 100%;
  width: 100%;
  font-family: ${props => props.theme.fonts.fontFamily};
`

export default StyledWindow