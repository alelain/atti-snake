import React, { PureComponent } from 'react'
import { map } from 'lodash'

import Block from '../Block'

import { withGameContext } from '../GameProvider'

const DEFAULT_INIT_SPEED = 250

class Snake extends PureComponent {

  state = {
    direction: 'r',
    speed: this.props.speed || DEFAULT_INIT_SPEED
  }

  componentDidMount() {
    const { speed } = this.state
    window.onkeydown = this.handleKeyPress
    this.snakeMoving = setInterval(this.goForward, speed)
  }

  componentDidUpdate() {
    const { setGameOver } = this.props
    setGameOver && setGameOver(this.isGameOver())
  }

  componentWillUnmount() {
    clearInterval(this.snakeMoving)
  }

  isGameOver = () => {
    const { windowMaxX, windowMaxY, tail, bHeight, bWidth } = this.props
    return (tail[0].x <= -bWidth || tail[0].x >= windowMaxX - bWidth || tail[0].y <= -bHeight || tail[0].y >= windowMaxY - bHeight) 
  }

  goForward = () => {
    const { direction } = this.state
    switch (direction) {
      case 'l':
        this.goToLeft()
        break;
      case 'r':
      this.goToRight()
        break;
      case 't':
        this.goToTop()
        break;
      case 'b':
        this.goToBottom()
        break;
      default:
        break;
    }
  }

  goToLeft = () => {
    const { bWidth, moveSnake } = this.props
    this.setState({ direction: 'l' }, () => {
      moveSnake && moveSnake(-bWidth, 0, 'l')
    })
  }

  goToRight = () => {
    const { bWidth, moveSnake } = this.props
    this.setState({ direction: 'r'}, () => {
      moveSnake && moveSnake(bWidth, 0, 'r')
    })
  }

  goToTop = () => {
    const { bHeight, moveSnake } = this.props
    this.setState({ direction: 't'}, () => {
      moveSnake && moveSnake(0, -bHeight, 't')
    })
  }

  goToBottom = () => {
    const { bHeight, moveSnake } = this.props
    this.setState({ direction: 'b' }, () => {
      moveSnake && moveSnake(0, bHeight, 'b')
    })
  }

  handleKeyPress = e => {
    const { direction } = this.state
    switch (e.keyCode) {
      case 38:
      case 90:
        direction !== 'b' && this.setState({ direction: 't' })
        break;
      case 37:
      case 81:
        direction !== 'r' && this.setState({ direction: 'l' })
        break;
      case 39:
      case 68:
        direction !== 'l' && this.setState({ direction: 'r' })
        break;
      case 40:
      case 83:
        direction !== 't' && this.setState({ direction: 'b' })
        break;
      default:
        break;
    }
  }

  snakeMoving = null

  renderTail = () => {
    const { bHeight, bWidth, tail } = this.props
    return map(tail, (block, index) => (
      <Block 
        key={index}
        width={bWidth} 
        height={bHeight} 
        x={block.x} 
        y={block.y} 
      />
    ))
  }

  render () {
    return this.renderTail()
  }
}

export default withGameContext(Snake)