import styled from 'styled-components'

const StyledScore = styled.span`
  color: #f2f2f2;
  font-family: Roboto;
  font-size: 1.5em;
  position: absolute;
  top: 5%;
  left: 5%;
`

export default StyledScore