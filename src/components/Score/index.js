import React, { PureComponent } from 'react'

import StyledScore from './styles/StyledScore'

class Score extends PureComponent {

  render() {
    const { score } = this.props

    return (
      <StyledScore>
        Score: {score}
      </StyledScore>
    )
  }
}

export default Score