import React, { Component } from 'react'

import StyledImage from './styles/StyledImage'

import { withGameContext } from '../GameProvider'

import appleSvg from './res/apple.svg'

class Apple extends Component {

  render () {
    const { 
      bHeight,
      bWidth,
      x,
      y,
    } = this.props

    return (
      <StyledImage height={bHeight} width={bWidth} x={x} y={y} src={appleSvg} alt="apple"/>
    )
  }
}

export default withGameContext(Apple)