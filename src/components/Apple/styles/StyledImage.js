import styled from 'styled-components'

const StyledImage = styled.img`
  width: ${props => props.width}px;
  height: ${props => props.height}px;
  position: absolute;
  left: ${props => props.x}px;
  top: ${props => props.y}px;
  z-index: 1;
`

export default StyledImage