import React, { Component } from 'react'

const GameContext = React.createContext({});

const withGameContext = WrappedComponent => {

  return class extends Component {
    render() {
      return (
        <GameContext.Consumer>
          {store => <WrappedComponent {...store} {...this.props} />}
        </GameContext.Consumer>
      )
    }
  }

}

export { GameContext, withGameContext }