import React, { Component } from 'react'
import StyledBlock from './styles/StyledBlock'

class Block extends Component {

  render () {
    const {
      height,
      width,
      x,
      y,
    } = this.props

    return (
      <StyledBlock 
        width={width} 
        height={height}
        x={x}
        y={y}
      />
    )
  }
}

export default Block