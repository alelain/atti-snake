import styled from 'styled-components'

const StyledBlock = styled.div`
  background-color:  ${props => props.color || "#f2f2f2"};
  width: ${props => props.width}px;
  height: ${props => props.height}px;
  position: absolute;
  left: ${props => props.x}px;
  top: ${props => props.y}px;
  border-radius: 24px;
  z-index: 2;
`

export default StyledBlock